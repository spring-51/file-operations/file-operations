## File Operation

### Postman collection

```
./postman/file-operations.postman_collection.json
```

#### Upload

```
1. Single File Upload
  - UploadController -> uploadSingle
  
2. Multiple File Upload
  - UploadController -> uploadMultiple
```

#### Download

```
1. Single File Download
  - for Image docs
  - DownloadController -> downloadSingleImage
  
  - for Pdf docs
  - DownloadController -> downloadSinglePdf
  
  - for Any docs
  - DownloadController -> downloadSingleAny
  
  - for Any docs and return json response in header
  - DownloadController -> downloadSingleAndJsonResponseAny
  
2. Multiple File Download
  - DownloadController -> downloadMultiple
```

#### Render

```
1. Single File Render
  - for Pdf docs
  - RenderController -> renderSinglePdf
  
  - for any docs
  - RenderController -> renderSingleAny
  
  
```