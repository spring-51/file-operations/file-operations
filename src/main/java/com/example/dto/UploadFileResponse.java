package com.example.dto;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UploadFileResponse {
    private String fileName;
    private String downloadUrl;
    private String contentType;
    private String renderUrl;
    private String request;
}
