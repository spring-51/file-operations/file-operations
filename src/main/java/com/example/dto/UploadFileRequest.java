package com.example.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Getter @Setter @ToString(exclude = {"file", "files"})
public class UploadFileRequest {
    private String name;
    /*
    for single file
     */
    private MultipartFile file;

    /*
    for multiple file
    the data type array or List both works
     */
    private List<MultipartFile> files;
}
