package com.example.common;

public class ApiUri {
    public static final String FORWARD_SLASH = "/";
    public static final String UPLOAD = FORWARD_SLASH + "upload";
    public static final String SINGLE = FORWARD_SLASH + "single";
    public static final String DOWNLOAD = FORWARD_SLASH + "download";
    public static final String RENDER = FORWARD_SLASH + "render";
    public static final String MULTIPLE = FORWARD_SLASH + "multiple";

}
