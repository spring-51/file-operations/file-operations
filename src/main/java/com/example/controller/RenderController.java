package com.example.controller;

import com.example.common.ApiUri;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping(ApiUri.RENDER)
public class RenderController {

    private Path fileStoragePath;
    private String fileStorageLocation;

    /*
    Constructor based autowiring
     */
    public RenderController(@Value("${file.storage.location:temp}") String fileStorageLocation) {
        this.fileStorageLocation = fileStorageLocation;
        fileStoragePath = Paths.get(fileStorageLocation).toAbsolutePath().normalize();

        try {
            Files.createDirectories(fileStoragePath);
        } catch (IOException e) {
            throw new RuntimeException("Issue in creating file directory");
        }
    }

    /**
     *  this api has same code as DownloadController -> downloadSingle*
     *  THE ONLY TWO DIFFERENCES ARE
     *  ===============================================================
     *  1. UNLIKE DownloadController -> downloadSingle*,
     *      while rendering contentType is MANDATORY HEADER
     *  ===============================================================
     *  2. last return statement
     *  in DownloadController -> downloadSingle*
     *      we are adding header(i.e. HttpHeaders.CONTENT_DISPOSITION) as attachment as
     *      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName="+resource.getFilename())
     *  where as here
     *     we are changing header HttpHeaders.CONTENT_DISPOSITION as inline as
     *      .header(HttpHeaders.CONTENT_DISPOSITION, "inline;fileName="+resource.getFilename())
     *  ===============================================================
     */
    @GetMapping(path = ApiUri.SINGLE + "/{fileName}/pdf")
    public ResponseEntity<?> renderSinglePdf(@PathVariable String fileName){

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());

        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){
            MediaType contentType = MediaType.APPLICATION_PDF;
            return ResponseEntity.ok()
                    /*
                    UNLIKE DownloadController -> downloadSingle*,
                    while rendering contentType is MANDATORY HEADER
                     */
                    .contentType(MediaType.parseMediaType(contentType.toString()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;fileName="+resource.getFilename())
                    .body(resource);
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }

    }

    /**
     *  this api has same code as DownloadController -> downloadSingle*
     *  THE ONLY TWO DIFFERENCES ARE
     *  ===============================================================
     *  1. UNLIKE DownloadController -> downloadSingle*,
     *      while rendering contentType is MANDATORY HEADER
     *  ===============================================================
     *  2. last return statement
     *  in DownloadController -> downloadSingle*
     *      we are adding header(i.e. HttpHeaders.CONTENT_DISPOSITION) as attachment as
     *      .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName="+resource.getFilename())
     *  where as here
     *     we are changing header HttpHeaders.CONTENT_DISPOSITION as inline as
     *      .header(HttpHeaders.CONTENT_DISPOSITION, "inline;fileName="+resource.getFilename())
     *  ===============================================================
     */
    @GetMapping(path = ApiUri.SINGLE + "/{fileName}/any")
    public ResponseEntity<?> renderSingleAny(
            @PathVariable String fileName,
            HttpServletRequest servletRequest) throws IOException {

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());

        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){

            String mimeType = servletRequest.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            System.out.println("File mimetype = " + mimeType);

            return ResponseEntity.ok()
                    /*
                    UNLIKE DownloadController -> downloadSingle*,
                    while rendering contentType is MANDATORY HEADER
                     */
                    .contentType(MediaType.parseMediaType(mimeType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "inline;fileName="+resource.getFilename())
                    .body(resource);
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }

    }


}
