package com.example.controller;

import com.example.common.ApiUri;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping(path = ApiUri.DOWNLOAD)
public class DownloadController {

    private Path fileStoragePath;
    private String fileStorageLocation;

    /*
    Constructor based autowiring
     */
    public DownloadController(@Value("${file.storage.location:temp}") String fileStorageLocation) {
        this.fileStorageLocation = fileStorageLocation;
        fileStoragePath = Paths.get(fileStorageLocation).toAbsolutePath().normalize();

        try {
            Files.createDirectories(fileStoragePath);
        } catch (IOException e) {
            throw new RuntimeException("Issue in creating file directory");
        }
    }

    @GetMapping(path = ApiUri.SINGLE + "/{fileName}/image")
    public ResponseEntity<?> downloadSingleImage(@PathVariable String fileName){

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());

        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){
            MediaType contentType = MediaType.IMAGE_JPEG;
            return ResponseEntity.ok()
                    /*
                    It looks like this contentType is not needed for CHROME BUT FOR POSTMAN contentType is mandatory field while downloading file
                    I have tested png, jpg, pdf file(Files are uploaded using UploadController -> uploadSingle)

                    BUT I NEED TO TEST MORE SCENARIOS

                    Conclusion :
                      It is recommended to use contentType, it helps client (like Angular, Postman, browsers  etc) to
                      download the fil with proper format
                     */
                    .contentType(MediaType.parseMediaType(contentType.toString()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName="+resource.getFilename())
                    .body(resource);
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }

    }

    @GetMapping(path = ApiUri.SINGLE + "/{fileName}/pdf")
    public ResponseEntity<?> downloadSinglePdf(@PathVariable String fileName){

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());

        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){
            MediaType contentType = MediaType.APPLICATION_PDF;
            return ResponseEntity.ok()
                    /*
                    It looks like this contentType is not needed for CHROME BUT FOR POSTMAN contentType is mandatory field while downloading file
                    I have tested png, jpg, pdf file(Files are uploaded using UploadController -> uploadSingle)

                    BUT I NEED TO TEST MORE SCENARIOS

                    Conclusion :
                      It is recommended to use contentType, it helps client (like Angular, Postman, browsers  etc) to
                      download the fil with proper format
                     */
                    .contentType(MediaType.parseMediaType(contentType.toString()))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName="+resource.getFilename())
                    .body(resource);
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }

    }

    @GetMapping(path = ApiUri.SINGLE + "/{fileName}/any")
    public ResponseEntity<?> downloadSingleAny(
            @PathVariable String fileName,
            HttpServletRequest servletRequest) throws IOException {

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());

        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){

            String mimeType = servletRequest.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            System.out.println("File mimetype = " + mimeType);

            return ResponseEntity.ok()
                    /*
                    It looks like this contentType is not needed for CHROME BUT FOR POSTMAN contentType is mandatory field while downloading file
                    I have tested png, jpg, pdf file(Files are uploaded using UploadController -> uploadSingle)

                    BUT I NEED TO TEST MORE SCENARIOS

                    Conclusion :
                      It is recommended to use contentType, it helps client (like Angular, Postman, browsers  etc) to
                      download the fil with proper format
                     */
                    .contentType(MediaType.parseMediaType(mimeType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName="+resource.getFilename())
                    .body(resource);
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }

    }

    /*
    refer
    - e.g without spring boot
    - https://stackoverflow.com/questions/57289988/provide-json-response-and-download-file-simultaneously-with-spring-boot
     */
    @GetMapping(path = ApiUri.SINGLE + "/{fileName}/file-and-json/any")
    public ResponseEntity<?> downloadSingleAndJsonResponseAny(
            @PathVariable String fileName,
            HttpServletRequest servletRequest) throws IOException {

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());
        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){

            String mimeType = servletRequest.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
            System.out.println("File mimetype = " + mimeType);

            Map<String, Object> map = new HashMap<>();
            map.put("String", "qwerty");
            map.put("Integer", 1);
            map.put("list", Arrays.asList("1", "2", "3"));

            return ResponseEntity.ok()
                    /*
                    It looks like this contentType is not needed for CHROME BUT FOR POSTMAN contentType is mandatory field while downloading file
                    I have tested png, jpg, pdf file(Files are uploaded using UploadController -> uploadSingle)

                    BUT I NEED TO TEST MORE SCENARIOS

                    Conclusion :
                      It is recommended to use contentType, it helps client (like Angular, Postman, browsers  etc) to
                      download the fil with proper format
                     */
                    .contentType(MediaType.parseMediaType(mimeType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;fileName="+resource.getFilename())
                    .header("jsonResponse", new ObjectMapper().writeValueAsString(map))
                    .body(resource);
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }

    }

    /*
    refer
    - e.g without spring boot
    - https://stackoverflow.com/questions/57289988/provide-json-response-and-download-file-simultaneously-with-spring-boot
     */
    @GetMapping(path = ApiUri.MULTIPLE)
    public void downloadMultiple(
            @RequestParam String[] files,
            HttpServletResponse servletResponse) throws IOException {

        // if we place it here its WORKING
        servletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=myZip.zip");

        try(ZipOutputStream zos = new ZipOutputStream(servletResponse.getOutputStream())){
            Arrays.stream(files)
                    .forEach(file -> {
                        Resource resource = downloadFileInternal(file);

                        ZipEntry zipEntry = new ZipEntry(resource.getFilename());

                        try {
                            zipEntry.setSize(resource.contentLength());

                            zos.putNextEntry(zipEntry);

                            StreamUtils.copy(resource.getInputStream(), zos);

                            zos.closeEntry();
                        } catch (IOException e) {
                            System.out.println("some exception while zipping");
                        }
                    })
            ;
            zos.finish();
        }

        // if we place it here its NOT WORKING
        // servletResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=download.zip");
    }

    private Resource downloadFileInternal(String fileName) {

        Path path = Paths.get(fileStorageLocation).toAbsolutePath().resolve(fileName);

        Resource resource;
        try {
            resource = new UrlResource(path.toUri());

        } catch (MalformedURLException e) {
            throw new RuntimeException("Issue in reading the file", e);
        }

        if(resource.exists() && resource.isReadable()){
            return resource;
        }else{
            throw new RuntimeException("the file doesn't exist or not readable");
        }
    }
}
