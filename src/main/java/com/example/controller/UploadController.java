package com.example.controller;

import com.example.common.ApiUri;
import com.example.dto.UploadFileRequest;
import com.example.dto.UploadFileResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

@RestController
@RequestMapping(path = ApiUri.UPLOAD)
public class UploadController {

    private Path fileStoragePath;
    private String fileStorageLocation;

    /*
    Constructor based autowiring
     */
    public UploadController(@Value("${file.storage.location:temp}") String fileStorageLocation) {
        this.fileStorageLocation = fileStorageLocation;
        fileStoragePath = Paths.get(fileStorageLocation).toAbsolutePath().normalize();

        try {
            Files.createDirectories(fileStoragePath);
        } catch (IOException e) {
            throw new RuntimeException("Issue in creating file directory");
        }
    }

    @PostMapping(path = ApiUri.SINGLE, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> uploadSingle(
            // https://stackoverflow.com/questions/23533355/spring-controller-requestbody-with-file-upload-is-it-possible
            UploadFileRequest request){
        if(ObjectUtils.isEmpty(request.getName())){
            throw  new RuntimeException("name is a mandatory field");
        }

        if(ObjectUtils.isEmpty(request.getFile())){
            throw  new RuntimeException("file is a mandatory field");
        }
        MultipartFile file = request.getFile();
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Path filePath = Paths.get(fileStoragePath + "\\" + fileName);

        try {
            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new RuntimeException("Issue in storing the file", e);
        }

        ///http://localhost:8081/download/single/abc.jpg
        String downloadUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(ApiUri.DOWNLOAD + ApiUri.SINGLE + ApiUri.FORWARD_SLASH)
                .path(fileName)
                .toUriString();

        ///http://localhost:8081/download/single/abc.jpg
        String renderUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(ApiUri.RENDER + ApiUri.SINGLE + ApiUri.FORWARD_SLASH)
                .path(fileName)
                .toUriString();

        String contentType = file.getContentType();

        UploadFileResponse response = new UploadFileResponse();
        response.setFileName(fileName);
        response.setDownloadUrl(downloadUrl);
        response.setContentType(contentType);
        response.setRenderUrl(renderUrl);
        response.setRequest(request.toString());
        return ResponseEntity.ok(response);

    }

    @PostMapping(path = ApiUri.MULTIPLE, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<?> uploadMultiple(
            // https://stackoverflow.com/questions/23533355/spring-controller-requestbody-with-file-upload-is-it-possible
            UploadFileRequest request
    ){
        if(ObjectUtils.isEmpty(request.getName())){
            throw  new RuntimeException("name is a mandatory field");
        }

        if(ObjectUtils.isEmpty(request.getFiles())){
            throw  new RuntimeException("files is a mandatory field");
        }
        List<UploadFileResponse> responses = new ArrayList<>();
        request.getFiles().forEach(file -> {
            String fileName = StringUtils.cleanPath(file.getOriginalFilename());
            Path filePath = Paths.get(fileStoragePath + "\\" + fileName);

            try {
                Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                throw new RuntimeException("Issue in storing the file", e);
            }

            ///http://localhost:8081/download/single/abc.jpg
            String downloadUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path(ApiUri.DOWNLOAD + ApiUri.SINGLE + ApiUri.FORWARD_SLASH)
                    .path(fileName)
                    .toUriString();

            ///http://localhost:8081/download/single/abc.jpg
            String renderUrl = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path(ApiUri.RENDER + ApiUri.SINGLE + ApiUri.FORWARD_SLASH)
                    .path(fileName)
                    .toUriString();

            String contentType = file.getContentType();

            UploadFileResponse response = new UploadFileResponse();
            response.setFileName(fileName);
            response.setDownloadUrl(downloadUrl);
            response.setContentType(contentType);
            response.setRenderUrl(renderUrl);
            response.setRequest(request.toString());
            responses.add(response);
        });

        Map<String, Object> finalResponse = new HashMap<>();
        finalResponse.put("list", responses);
        return ResponseEntity.ok(finalResponse);

    }
}
